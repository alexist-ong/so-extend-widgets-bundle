<?php
/*
  Plugin Name:          Extra Widgets for SiteOrigin
  Plugin URI:           https://gitlab.com/ubicompsystem/so-extend-widgets-bundle
  Description:          Additional custom made SiteOrigin widgets
  Version:              3.1.0
  Author:               Alexist Ong
  Author URI:           https://gitlab.com/ubicompsystem/
  License:              GPL2
  License URI:          https://www.gnu.org/licenses/gpl-2.0.html
  GitLab Plugin URI:    https://gitlab.com/ubicompsystem/so-extend-widgets-bundle
 */

function add_extra_siteorigin_widgets_collection($folders){
    $folders[] = plugin_dir_path(__FILE__).'so-extra-widgets/';
    return $folders;
}
add_filter('siteorigin_widgets_widget_folders', 'add_extra_siteorigin_widgets_collection');